var express = require('express');
var router = express.Router();
var modules    = require('./modules');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('information');
});

router.post('/save_info', function(req, res, next) {
    question_number = 1;
    result = {e:0, i:0, l:0};
    modules.next_page(question_number, result, res);
});


module.exports = router;
