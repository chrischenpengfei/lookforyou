var Firebase = require("firebase");

this.next_page = function(question_number, result, res) {
	console.log(question_number);
	console.log(result);
	url = "https://question-list.firebaseio.com/" + question_number;
    var myFirebaseRef = new Firebase(url);
    myFirebaseRef.on("value", function(snapshot) {
            title = "Look For You"
            values = snapshot.val();
            console.log(values);
            answer1 = values.answer1;
            answer2 = values.answer2;
            answer3 = values.answer3;
            console.log(answer3)
            question = values.question;
            topic = values.topic;
            res.render('questions', {title: title, question_number: question_number, question: question, answer1: answer1, answer2: answer2, answer3: answer3, topic: topic, result: result});
    });
};

this.calculate_result = function(result, res){
    res.render('result');
};