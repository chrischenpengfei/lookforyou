var express = require('express');
var router = express.Router();
var modules    = require('./modules');

/* GET home page. */
router.get('/', function(req, res, next) {
    question_number = 1;
    result = {e:0, i:0, l:0};
    modules.next_page(question_number, result, res);
});

router.get('/next', function(req, res, next) {
    result = req.query['result'];
    number = req.query['num'];

    if (number < 20) { 
        choice_value = req.query['radio-choice'];
        if ('emotion' == req.query['topic']) {
            result['e'] = result['e'] + parseInt(choice_value)
        } else if ('intelligency' == req.query['topic']) {
            result['i'] = result['i'] + parseInt(choice_value)
        } else {
            result['l'] = result['l'] + parseInt(choice_value)
        };
        question_number = parseInt(number) + 1;
        new_question_number = question_number.toString();
        modules.next_page(question_number, result, res);
    } else {
        modules.calculate_result(result, res);
    }
});

module.exports = router;
